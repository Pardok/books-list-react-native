import React from 'react';
import { View, FlatList, StyleSheet, Image } from 'react-native';
import { Text, TouchableRipple, withTheme, Divider, ActivityIndicator} from 'react-native-paper';

const BooksList = function ({ navigation, books, customEmptyComponent }) {

    const Loading = <ActivityIndicator style={{ flexGrow: 1 }} size="large" />

    const Item = ( {item} ) => (
        <TouchableRipple onPress={()=>{
            setTimeout(() => {
                navigation.navigate('Book', {item: item})
            }, 0);
        }}>
            <View style={listStyles.itemContainer}>
                <View style={listStyles.imageWrapper}>
                    <Image resizeMode='contain' style={listStyles.thumbnail} source={{uri: item.thumbnailUrl}}/>
                </View>
                <View style={listStyles.textContainer}>
                    <Text style={listStyles.title} adjustsFontSizeToFit>{item.title}</Text>
                    <Text style={listStyles.description} numberOfLines={3} ellipsizeMode='tail'>{item.shortDescription ? item.shortDescription : item.longDescription}</Text>
                </View>
            </View>
        </TouchableRipple>
    )

    const renderItem = ( { item, index } ) => {
        return (
            <Item item={item}/>
        )
    }

    return (
        <View style={listStyles.container}>
            <FlatList
                keyExtractor={(item, index) => item.title+'/isbn:'+item.isbn}
                data={
                    books
                }
                renderItem={renderItem}
                contentContainerStyle={{ flexGrow: 1 }}
                ListEmptyComponent={customEmptyComponent || Loading}
                ItemSeparatorComponent={Divider}
            />
        </View>
    )
}

const sizes = {
    image: {
        width: 64,
        height: 64
    }
}

const listStyles = StyleSheet.create({
    container: {
        flex: 1
    },
    itemContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    imageWrapper: {
        width: sizes.image.width,
        height: sizes.image.height,
        margin: 8
    },
    thumbnail: {
        height: sizes.image.height
    },
    textContainer: {
        flexDirection: 'column',
        flex: 1,
        margin: 8
    },
    title: {
        fontSize: 18,
        fontFamily: 'Roboto_500Medium'
    },
    description: {
        fontFamily: 'Roboto_400Regular',
        flexGrow: 1
    }
})

export default withTheme(BooksList)