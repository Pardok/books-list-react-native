import React, { useState } from 'react';
import { SafeAreaView, View, StatusBar } from 'react-native';
import { observable } from "mobx"

import AsyncStorage from '@react-native-community/async-storage';

import { NavigationContainer } from '@react-navigation/native';
import { createMaterialBottomTabNavigator  } from '@react-navigation/material-bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';

import { DefaultTheme, DarkTheme, Provider as PaperProvider, ActivityIndicator, Snackbar } from 'react-native-paper';

import { MaterialIcons as Icon, MaterialCommunityIcons as CIcon } from '@expo/vector-icons';
import { useFonts, Roboto_400Regular, Roboto_500Medium, Roboto_700Bold } from '@expo-google-fonts/roboto';

import CatalogScreen    from './screens/Catalog'
import FavoritesScreen  from './screens/Favorites'
import BookScreen       from './screens/Book'

const defaultTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#0d47a1',
    accent: '#5472d3',
  },
};

const darkTheme = {
  ...DarkTheme,
  colors: {
    ...DarkTheme.colors,
    primary: '#5472d3',
    accent: '#0d47a1',
  },
};


let books = require('./books.json')
// We can use Set here, but it's faster to access favorites by isbn key.
// We accept it as unique identifier (copies have same), in other case
// it can be url of book page in online shop or etc.
const booksFiltered = new Map(books.map(i => [i.isbn, i]));
books = Array.from(booksFiltered.values())

let dataLoaded = observable.box(false);
let favorites = observable(new Set())

// this implementation can't be used for multiple simultaneous loading
const getData = async (key) => {
  try {
    dataLoaded = false
    const jsonValue = await AsyncStorage.getItem(key)
    return jsonValue != null ? JSON.parse(jsonValue) : null;
  } catch(e) {
    console.warn(e)
  }
}

getData('@books_Favorites').then(data=>{
  Array.isArray(data) ? favorites = observable(new Set(data)) : null; dataLoaded = true
})

const Tab = createMaterialBottomTabNavigator();
const Stack = createStackNavigator();

const StackNavigatedBook = (props) => (
  <View style={{flex: 1}}>
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name={"Tabs"} children={()=>props.children} />
      <Stack.Screen name={"Book"} children={ newProps => <BookScreen {...newProps} favorites={props.favorites} /> } />
    </Stack.Navigator>
  </View>
)

export default function App () {
  const [currentTheme, useTheme] = useState(defaultTheme);

  let [fontsLoaded] = useFonts({
    Roboto_400Regular,
    Roboto_500Medium,
    Roboto_700Bold
  });

  return fontsLoaded && dataLoaded ? (
    <PaperProvider theme={currentTheme}>
      <StatusBar backgroundColor={currentTheme.colors.background} barStyle={currentTheme.dark ? 'light-content' : 'dark-content'} />
      <SafeAreaView style={{ flex: 1 }}>
        <NavigationContainer theme={currentTheme}>
          <StackNavigatedBook favorites={favorites}>
            <Tab.Navigator screenOptions={{}} shifting>
              <Tab.Screen name="Catalog"    children={ props => 
                  <CatalogScreen    {...props} books={books}/>
              } options={{
                tabBarIcon: ({focused, color}) => (focused ?
                  <Icon   name='book'         color={color} size={24} /> :
                  <CIcon  name='book-outline' color={color} size={24} />)
              }}/>
              <Tab.Screen name="Favorites"   children={ props =>
                  <FavoritesScreen   {...props} books={books} booksFiltered={booksFiltered} favorites={favorites}/>
              } options={{
                tabBarIcon: ({focused, color}) => (
                  <Icon name={focused ? 'favorite' : 'favorite-border'} color={color} size={24} />)
              }}/>
            </Tab.Navigator>
          </StackNavigatedBook>
        </NavigationContainer>
      </SafeAreaView>
    </PaperProvider>
  ) : <ActivityIndicator style={{ flexGrow: 1 }} size="large" />;
}