import React from 'react';
import { Card, Button, Title, Paragraph, withTheme } from 'react-native-paper';
import { observer } from "mobx-react"

import BooksList from '../components/List'


const FavoritesScreen = observer((props) => {

    let books = Array.from(props.favorites.values()).map(key => props.booksFiltered.get(key))

    const EmptyList = 
        <Card style={{ margin: 8 }}>
            <Card.Content>
                <Title>Add something to favorite</Title>
                <Paragraph>Click on any book, then click on 3 dots sign and select "Add to favorite"</Paragraph>
            </Card.Content>
            <Card.Actions>
                <Button onPress={() => props.navigation.navigate('Catalog')}>Go to Catalog</Button>
            </Card.Actions>
        </Card>

    return (
        <BooksList {...props} books={books} customEmptyComponent={EmptyList}/>
    );
})

export default withTheme(FavoritesScreen)