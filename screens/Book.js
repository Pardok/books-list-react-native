import React from 'react';
import { View, StyleSheet, Image, ScrollView } from 'react-native';
import { Button, Text, withTheme, IconButton, Modal, Portal, Snackbar } from 'react-native-paper';
import { observer } from "mobx-react"

import AsyncStorage from '@react-native-community/async-storage';

const storeData = async (value, key) => {
    try {
        const jsonValue = JSON.stringify(value)
        await AsyncStorage.setItem(key, jsonValue)
    } catch (e) {
        console.warn(e)
    }
}
  

const BookScreen = observer(({ navigation, favorites, route: { params: { item } } }) => {
    const [modalVisible, setModalVisible] = React.useState(false)

    const showModal = () => setModalVisible(true)
    const hideModal = () => setModalVisible(false)

    const [snackVisible, setSnackVisible] = React.useState(false)
    const [snackText, setSnackText] = React.useState('')

    const showSnack = (text = undefined) => {
        text ? setSnackText(text) : null;
        setSnackVisible(true)
    }
    const hideSnack = () => setSnackVisible(false)


    const isFavorite = favorites.has(item.isbn)

    const changeFavorite = () => {
        hideModal();
        favorites.has(item.isbn) ? favorites.delete(item.isbn) : favorites.add(item.isbn)
        showSnack(`"${item.title}" ${!isFavorite ? 'added to' : 'removed from'} favorites`)
        storeData([...favorites], '@books_Favorites').then(() => {
            showSnack(`"${item.title}" ${!isFavorite ? 'added to' : 'removed from'} favorites.\nFavorites saved`)
        })
    }
    
    const ImageBlock = () => (
        <View style={bookStyles.imageBlockContainer}>
            <Image style={bookStyles.imageSize} resizeMode='contain' source={{uri: item.thumbnailUrl}}/>
            <Image style={bookStyles.gradient} resizeMode='stretch' source={require('../assets/gradient.png')} />
            <View style={bookStyles.titleWrapper}>
                <Text style={bookStyles.title}>{item.title}</Text>
                <IconButton icon='dots-vertical' color='white' size={26} onPress={showModal}/>
            </View>
            <IconButton style={bookStyles.backButton} icon='arrow-left' color='white' size={26} onPress={navigation.goBack}/>
        </View>
    )

    const ActionSheet = () => (
        <Portal>
            <Modal contentContainerStyle={bookStyles.modalContainer} visible={modalVisible} onDismiss={hideModal}>
                <Button style={bookStyles.modalButton} onPress={hideModal}>Cancel</Button>
                <Button style={bookStyles.modalButton} onPress={changeFavorite}>
                    {isFavorite ? 'Remove from favorites' : 'Add to favorites'}
                </Button>
            </Modal>
        </Portal>
    )
    
    return (
        <View style={bookStyles.container}>
            <ImageBlock {...item}/>
            <ScrollView style={bookStyles.textScrollWrapper}>
                <Text style={bookStyles.description} ellipsizeMode='tail'>
                    {item.longDescription ? item.longDescription : item.shortDescription}
                </Text>
            </ScrollView>
            <ActionSheet/>
            <Snackbar
                visible={snackVisible}
                onDismiss={hideSnack}
                duration={3000}
                action={{
                    label: 'OK',
                    onPress: hideSnack,
                }}>
                {snackText}
            </Snackbar>
        </View>
        
    )
})

const sizes = {
    imageHeight: 200
}

const bookStyles = StyleSheet.create({
    container: {
        flex: 1
    },
    leftArrow: {
        position: 'absolute'
    },
    imageBlockContainer: {
        width: '100%',
        backgroundColor: 'lightgray',
        position: 'relative'
    },
    imageSize: {
        height: sizes.imageHeight
    },
    gradient: {
        height: sizes.imageHeight / 2,
        position: 'absolute',
        bottom: 0
    },
    titleWrapper: {
        position: 'absolute',
        flexDirection: 'row',
        bottom: 0,
        margin: 8
    },
    title: {
        textAlignVertical: 'center',
        flex: 1,
        fontSize: 20,
        fontFamily: 'Roboto_500Medium',
        color: 'white'
    },
    backButton: {
        position: 'absolute',
        backgroundColor: `rgba(128,128,128,${1/8})`
    },
    textScrollWrapper: {
        flexGrow: 1,
        flexDirection: 'column'
    },
    description: {
        fontFamily: 'Roboto_400Regular',
        margin: 8
    },
    modalContainer: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
        flexDirection: 'column-reverse',
        justifyContent: 'flex-start'
    },
    modalButton: {
        margin: 8,
        borderRadius: 20,
        backgroundColor: 'white'
    }
})

export default withTheme(BookScreen)