import React from 'react';
import { withTheme } from 'react-native-paper';

import BooksList from '../components/List'


const CatalogScreen = function (props) {
    return (
        <BooksList {...props}/>
    );
}

export default withTheme(CatalogScreen)